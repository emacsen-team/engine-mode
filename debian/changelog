engine-mode (2.2.4-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 20:14:30 +0900

engine-mode (2.2.4-1) unstable; urgency=medium

  * New upstream version 2.2.4
  * Install proper documentation
  * Refresh patch to clean documentation
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 24 Jun 2023 10:50:31 +0500

engine-mode (2.2.1-1) unstable; urgency=medium

  * New upstream version 2.2.1
  * Add upstream metadata
  * Refresh patch to clean documentation
  * d/control: Declare Standards-Version 4.6.2 (no changes needed)
  * d/control: Migrate to debhelper-compat 13
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Wed, 21 Dec 2022 14:01:20 +0500

engine-mode (2.1.1-1) unstable; urgency=medium

  [ David Krauser ]
  * d/control: Update maintainer email address

  [ Lev Lamberov ]
  * New upstream version 2.1.1
  * Migrate to debhelper 12 without d/compat
  * Refresh patch to clean documentation
  * Drop patch to fix upstream version
  * d/control: Declare Standards-Version 4.4.1 (no changes needed)
  * d/control: Add Rules-Requires-Root: no
  * d/control: Clean Depends and Enhances
  * d/control: Drop Built-Using field
  * d/copyright: Bump copyright years
  * d/rules: Do not pass --parallel to dh

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 03 Dec 2019 14:18:26 +0500

engine-mode (2.0.0-4) unstable; urgency=medium

  * Team upload.
  * Regenerate source package with quilt patches

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 15:01:56 -0300

engine-mode (2.0.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Sun, 25 Aug 2019 09:35:57 -0300

engine-mode (2.0.0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 20:46:35 -0300

engine-mode (2.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #849625)

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 29 Dec 2016 12:07:14 +0500
